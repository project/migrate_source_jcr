<?php

namespace Drupal\migrate_source_jcr\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Row;
use Jackalope\RepositoryFactoryJackrabbit;
use PHPCR\PathNotFoundException;
use PHPCR\SimpleCredentials;
use PHPCR\UnsupportedRepositoryOperationException;

/**
 * Source plugin for importing JCR data.
 *
 * Available configuration options:
 * - host: The host and port of the JCR server, default is localhost port 4502.
 * - query: The query string for the target nodes.
 * - type: The query type, possible values include: JCR-SQL, JCR-SQL2, XPATH.
 * - user: The username of the JCR instance.
 * - pass: The password of the JCR instance.
 * - workspace: The name of the JCR workspace.
 * - batch_size: (optional) The number of nodes processed per batch.
 *
 * Example with required options configured:
 * @code
 * source:
 *   plugin: jcr
 *   host: "http://localhost:4502/crx/server/"
 *   query: 'SELECT * FROM [nt:unstructured] AS node WHERE ISDESCENDANTNODE(node, "/content/blog/posts")
 *           AND [sling:resourceType] = "blog/templates/post"'
 *   type: "JCR-SQL2"
 *   user: "admin"
 *   pass: "admin"
 *   workspace: "crx.default"
 *   fields:
 *     -
 *       name: "title"
 *       subpath: ""
 *       property: "jcr:title"
 *     -
 *       name: "body"
 *       subpath: "par/text"
 *       property: "rich_text"
 * @endcode
 *
 * In the example above, the "jcr:title" property can be found on nodes
 * returned by the query and the "rich_text" property can be found on
 * descendant nodes found at path relative to the main node (in this case
 * "par/text"). The "name" defined in each field will be property where the
 * property's data will be stored on the row.
 *
 * @see https://phpcr.readthedocs.io/en/latest/
 * @see https://github.com/jackalope/jackalope-jackrabbit/blob/master/README.md
 * @see https://www.drupal.org/docs/8/api/migrate-api/migrate-source-plugins/migrating-data-from-a-jcr-source
 *
 * @MigrateSource(
 *   id = "jcr",
 *   source_module = "migrate_source_jcr"
 * )
 */
class JCR extends SourcePluginBase {

  /**
   * Information on the source fields to be extracted from the data.
   *
   * @var array[]
   *   Array of field information keyed by field names. A 'label' subkey
   *   describes the field for migration tools; a 'path' subkey provides the
   *   source-specific path for obtaining the value.
   */
  protected $fields = [];

  /**
   * The Jackrabbit repository factory.
   *
   * @var \Jackalope\RepositoryFactoryJackrabbit
   */
  protected $jackalopeFactory;

  /**
   * The Jackrabbit URL.
   *
   * @var string
   */
  protected $host;

  /**
   * The query.
   *
   * @var \PHPCR\Query\QueryInterface
   */
  protected $query;

  /**
   * The query string.
   *
   * @var string
   */
  protected $queryString;

  /**
   * The query type.
   *
   * @var string
   */
  protected $queryType;

  /**
   * The CRX username.
   *
   * @var string
   */
  protected $user = 'admin';

  /**
   * The CRX password.
   *
   * @var string
   */
  protected $pass = 'admin';

  /**
   * The CRX workspace.
   *
   * @var string
   */
  protected $workspace = 'crx.default';

  /**
   * The batch size.
   *
   * @var int
   */
  protected $batchSize = 0;

  /**
   * The current batch.
   *
   * @var int
   */
  protected $batch = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->fields = $configuration['fields'];
    $this->jackalopeFactory = new RepositoryFactoryJackrabbit();
    $this->host = $configuration['host'];
    $this->queryString = $configuration['query'];
    $this->queryType = $configuration['type'];
    $this->user = $configuration['user'];
    $this->pass = $configuration['pass'];
    $this->workspace = $configuration['workspace'];
    $this->batchSize = $configuration['batch_size'];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return "JCR data";
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    // If batch is 0, no rows have been processed, so build the initial query.
    if ($this->batch == 0) {
      $this->buildQuery();
    }

    // If the batchSize has been set in the migration yml, we offset the query
    // to retrieve the appropriate results for the current batch.
    if ($this->batchSize > 0) {
      $offset = $this->batch * $this->batchSize;
      $this->query->setOffset($offset);
    }

    $queryResult = $this->query->execute();

    $nodesArray = [];

    // Although paths are unique in JCR, they may be too long to store as ids
    // in the database. Instead, we use md5 to create a 10-character hash of
    // each path to use as an id.
    foreach ($queryResult->getNodes() as $path => $node) {
      $nodesArray[] = ['node' => $node, 'path_hash' => md5($path)];
    }

    $nodesObject = new \ArrayObject($nodesArray);

    return $nodesObject->getIterator();
  }

  /**
   * Position the iterator to the following row.
   */
  protected function fetchNextRow() {
    $this->getIterator()->next();
    // The line above is moving to the next position in the iterator,
    // even if there aren’t any more elements left in it.
    // The valid check makes sure the position in the iterator is valid.
    // In this case, if the position in the iterator is not valid,
    // that signifies that we’ve exhausted the batch and there’s
    // no longer anything to iterate over. So we go get the next batch.
    if ($this->batchSize > 0 && !$this->getIterator()->valid()) {
      $this->fetchNextBatch();
    }
  }

  /**
   * Prepares query for the next set of data from the source database.
   */
  protected function fetchNextBatch() {
    $this->batch++;
    // Unset the iterator so that getIteratator uses the new query.
    // @see getIterator()
    unset($this->iterator);
    $this->getIterator()->rewind();
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    /** @var \Jackalope\Node $node */
    $node = $row->getSourceProperty('node');

    foreach ($this->fields as $field_info) {
      try {

        // If the field definition has a subpath, get the descendant node that
        // exists at that subpath. Then set the row property as the value of
        // the property on the descendant node.
        /** @var \Jackalope\Node $propertyElement */
        $propertyElement = ('' === $field_info['subpath']) ? $node : $node->getNode($field_info['subpath']);
        $value = $propertyElement->getPropertyValue($field_info['property']);

        $row->setSourceProperty($field_info['name'], $value);

      }
      catch (PathNotFoundException $e) {
        // A field defined in config on doesn't exist at a specified path. Set
        // the property on the row to NULL.
        $row->setSourceProperty($field_info['name'], NULL);

      }
      catch (UnsupportedRepositoryOperationException $e) {
        // A field defined in config corresponds to a property on a
        // non-existent descendant node. Set the property on the row to NULL.
        $row->setSourceProperty($field_info['name'], NULL);
      }
      catch (\Exception $e) {
        throw new MigrateException($e->getMessage());
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['path_hash']['type'] = 'string';
    return $ids;
  }

  /**
   * Builds the query with parameters defined in the migration configuration.
   */
  protected function buildQuery() {
    $repository = $this->jackalopeFactory->getRepository(
      ["jackalope.jackrabbit_uri" => $this->host]
    );

    $credentials = new SimpleCredentials($this->user, $this->pass);
    $session = $repository->login($credentials, $this->workspace);
    $workspace = $session->getWorkspace();
    $queryManager = $workspace->getQueryManager();
    $query = $queryManager->createQuery($this->queryString, $this->queryType);

    // If there was a defined batch size, limit the amount of nodes
    // returned by the query accordingly.
    if ($this->batchSize > 0) {
      $query->setLimit($this->batchSize);
    }

    $this->query = $query;
  }

}
