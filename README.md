CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Recommended Modules
* Resources
* Roadmap
* Known Problems
* Contributing

INTRODUCTION
------------

The Migrate Source JCR module provides a source plugin for migrating from Java
Content Repository (JCR) storage. It leverages [Jackalope], an implementation
of PHPCR, for queries.

The general approach for using this plugin is:

1. Allow access to the JCR instance from Drupal.
2. Write a Drupal migration using the JCR source plugin.

Writing the migrations will require a working knowledge of how JCR data is
structured and how to map it to the structure inside Drupal. Refer to the
Resources below and related Documentation for help.

REQUIREMENTS
------------

There are no additional requirements for using the JCR migration plugin.
However, making sure the JCR source is setup and accessible to Drupal may have
its own considerations.

INSTALLATION
------------

This module is installed similar to other Drupal modules. Because there are
composer dependencies it is important that this module be installed via
composer. After requiring the module with composer, enable the module on the
site.

CONFIGURATION
-------------

There is no additional configuration required to use the JCR source plugin.

USAGE
-----

Please refer to the online documentation for detailed information about its
usage: [Migrating data from a JCR source].

RECOMMENDED MODULES
-------------------

These two modules are useful for working with migrations:

* [Migrate Plus] - Allows migration plugins to be implemented as configuration
  entities, allowing them to flexibly be loaded, modified, and saved.
* [Migrate Tools] - Provides the 'migrate:import' Drush command that you can
  use to run the migration from the command line.

Given the complex nature of migrating from JCR it may be worth considering the
use of a hierarchical data structure in Drupal using a module like Paragraphs
and migrating into nested paragraph types.

RESOURCES
---------

* #migration in the [Drupal Slack]
* [Jackalope documentation], which links to the PHPCR Book
* [The PHPCR Book], for understanding in detail how JCR queries work
* Magnolia CMS' [JCR Query Cheat Sheet]
* Paul Rohrbeck’s [Query Builder Cheat Sheet]
* [Wikipedia's JCR page], shows a list of possible migration sources

ROADMAP
-------

There are no planned improvements or changes. However, feel welcome to share
ideas in the issue queue.

KNOWN PROBLEMS
--------------

* During a migration it may run out of memory, a temporary work around is to
  repeat the migration until it passes; see [#2701121]
* Paragraphs only: [#3075039 Ability to map paragraphs to an entity]
* Paragraphs only: [#3075041 Ability to map subparagraphs to a paragraph]

CONTRIBUTING
------------

Migrating from JCR storage to Drupal is a complex process and having tangible
examples for migrations would help us understand better what opportunities
there are for improving it. If you are using Migrate Source JCR on a project,
share with the community how you're using it and what opportunities there are
for improving it by [submitting a case study].

If you have a bug or issue, please always feel welcome to reach out in the [issue queue].

<!--
This README should be kept in sync with the project page:
https://www.drupal.org/project/issues/migrate_source_jcr

An easy way to do this is to use pandoc and convert it like so:
pandoc README.md -r markdown -w html -o README.html
-->

[Jackalope]: https://jackalope.github.io/
[Migrating data from a JCR source]: https://www.drupal.org/docs/8/api/migrate-api/migrate-source-plugins/migrating-data-from-a-jcr-source
[Migrate Plus]: https://www.drupal.org/project/migrate_plus
[Migrate Tools]: https://www.drupal.org/project/migrate_plus
[Drupal Slack]: https://www.drupal.org/slack
[Jackalope documentation]: https://github.com/jackalope/jackalope/wiki/Documentation
[The PHPCR Book]: https://phpcr.readthedocs.io/en/latest/book/
[Wikipedia's JCR page]: https://en.wikipedia.org/wiki/Content_repository_API_for_Java
[JCR Query Cheat Sheet]: https://wiki.magnolia-cms.com/display/WIKI/JCR+Query+Cheat+Sheet
[Query Builder Cheat Sheet]: https://github.com/paulrohrbeck/aem-links/blob/master/querybuilder_cheatsheet.md
[issue queue]: https://www.drupal.org/project/issues/migrate_source_jcr
[#2701121]: https://www.drupal.org/project/migrate_tools/issues/2701121
[#3075039 Ability to map paragraphs to an entity]: https://www.drupal.org/project/migrate_source_jcr/issues/3075039
[#3075041 Ability to map subparagraphs to a paragraph]: https://www.drupal.org/project/migrate_source_jcr/issues/3075041
[submitting a case study]: https://www.drupal.org/node/add/project-issue/migrate_source_jcr?component=Case%20study&categories=2
